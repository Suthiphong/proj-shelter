import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MapSearchService} from "../../services/map-search.service";
import Point from "@arcgis/core/geometry/Point";
import Graphic from "@arcgis/core/Graphic";

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.css']
})
export class SearchPageComponent implements OnInit {

  @ViewChild('mapDiv') mapDiv: ElementRef;
  searchByName = '';
  searchByCap = '';

  constructor(private mapSearchService: MapSearchService) {
  }


  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.mapSearchService.initialMap(this.mapDiv)
  }

  async onSearch() {
    let params = this.mapSearchService.featureLayer.createQuery();
    params.where = `facname='${this.searchByName}' OR sheltcap=${Number(this.searchByCap)}`

    params.outFields = ["*"]
    params.returnGeometry = true

    const response = await this.mapSearchService.featureLayer.queryFeatures(params)

    if (response.features.length > 0) {
      const feature = response.features[0]
      const point = feature.geometry as Point

      let symbol = {
        type: "simple-marker",  // autocasts as new SimpleMarkerSymbol()
        style: "circle",
        color: [100, 0, 0, 0.3],
        size: "40px",  // pixels
        outline: {  // autocasts as new SimpleLineSymbol()
          color: [100, 0, 0],
          width: 1  // points
        }
      };

      const graphic = new Graphic({
        geometry: point,
        symbol: symbol
      })

      this.mapSearchService.mapView.graphics.removeAll()
      this.mapSearchService.mapView.graphics.add(graphic)

      this.mapSearchService.mapView.goTo({
        center: [point.longitude, point.latitude]
      })

      this.mapSearchService.mapView.popup.open({
        location: point,
        features: [feature]
      })
    }
  }

}
