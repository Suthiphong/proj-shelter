import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MapService} from "../../services/map.service";

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})
export class RegisterPageComponent implements OnInit {

  @ViewChild('mapDiv') mapDiv:ElementRef;
  constructor(private mapService: MapService) { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.mapService.initialMap(this.mapDiv);
  }

}
