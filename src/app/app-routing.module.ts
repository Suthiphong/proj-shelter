import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RegisterPageComponent} from "./page/register-page/register-page.component";
import {SearchPageComponent} from "./page/search-page/search-page.component";

const routes: Routes = [
  {path: 'register-page', component: RegisterPageComponent},
  {path: 'search-page', component: SearchPageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


